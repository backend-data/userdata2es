import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.DeleteObjectRequest;

/**
 * Created by xm010 on 10/15/15.
 */
object FacebookData {

  def loadFBData(sqlContext: SQLContext, path: String) = {

    import sqlContext.implicits._
    import org.apache.spark.sql.functions.udf
    

//    val pkey = RSA.readPrivateKey(RSA.privateKey).get
//    val decrypt = udf((str: String) => RSA.decrypt(str, pkey))
    

    val jdbcDF = sqlContext.read.format("jdbc").options(
      Map("url" -> "jdbc:mysql://172.31.15.181:3306/keyboard?user=mosh&password=123456",
        "dbtable" -> "t_sdk_user_info",
        "driver" -> "com.mysql.jdbc.Driver"
      )
    ).load()

    val jdbcDF1 = sqlContext.read.format("jdbc").options(
      Map("url" -> "jdbc:mysql://172.31.15.181:3306/keyboard?user=mosh&password=123456",
        "dbtable" -> "t_sdk_user_info"
      )
    ).load()

//    val jdbc = jdbcDF
//      //.select("id", "device_uid", "account_id", decrypt('account_info) as 'fb, "package_name", "source")
////      .select('id, 'device_uid, 'account_id, decrypt('account_info) as 'fb, 'package_name, 'source).limit(100)
//    .select('id, 'device_uid, 'account_id, decrypt('account_info))
    
        
    val jdbc = jdbcDF  
    .select('id, 'device_uid, 'account_id, 'account_info)
    .map{x => 
      
      
      
      var accountinfo = ""
      
      try
      {
        val pkey = RSA.readPrivateKey(RSA.privateKey).get
        accountinfo = RSA.decrypt(x(3).toString(), pkey)
      }
      catch
      {
        case _: Throwable =>
          accountinfo = ""
      }
      
      (x(0), x(1),x(2),accountinfo)
      
    }
    .filter{x => x._4 != ""}
    
    
      //.where(($"can_preload" === 1 or $"can_preload" === 2) and $"pkgname".isNotNull)
      //.distinct
      //.orderBy(s"id")

    println(s"#### count ${jdbc.count()}")
//    println(s"#### first ${jdbc.first()}")

//    jdbc.write.parquet(path)
    
    jdbc.saveAsTextFile(path)

  }

  def main(args: Array[String]) {

    val sc = new SparkContext()
    val sqlContext = new SQLContext(sc)

    
    val hadoopConf = sc.hadoopConfiguration;
    
      hadoopConf.set("fs.s3n.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")

      hadoopConf.set("fs.s3n.awsAccessKeyId", "AKIAJIVCMRHJ56IM7IVA")
    
      hadoopConf.set("fs.s3n.awsSecretAccessKey", "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")
    
      
      
      
     val path = "s3n://xinmei-dataanalysis/fb/"
     AWS.removeFile("fb")
    loadFBData(sqlContext, path)
    
    
//    val haha = sc.parallelize(Array(1, 2, 3, 4, 5))
//    
//    
//    haha.repartition(1).saveAsTextFile(path)
//    
//    val s3client = new AmazonS3Client(new ProfileCredentialsProvider())
//    
    
     
    
    sc.stop()

  }

}
