import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.elasticsearch.spark._

import scala.collection.immutable.Map


/**
 * @author xm002
 */


//倒入theme用户的全量用户



object themeJoinInputTest {


  def main(args: Array[String]) {

    val conf = new SparkConf()
    conf.set("es.index.auto.create", "false")
      .set("es.nodes", "172.31.29.243")
      .set("es.mapping.id", "key")
      .set("es.batch.size.entries", "1")
    //      .set("es.write.operation","upsert")


    val sc = new SparkContext(conf)

    val hadoopConf = sc.hadoopConfiguration
    hadoopConf.set("fs.s3n.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
    hadoopConf.set("fs.s3n.awsAccessKeyId", "AKIAJIVCMRHJ56IM7IVA")
    hadoopConf.set("fs.s3n.awsSecretAccessKey", "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")





    val themeAppKeys = Array("d7b10839af1ba26bc4b64881501e6df0", "6afea8f14e72656cf01cf876cf7a4391") // theme keys
    val appKeys = Array("78472ddd7528bcacc15725a16aeec190", "e2934742f9d3b8ef2b59806a041ab389", "4e5ab3a6d2140457e0423a28a094b1fd", "34c0ab0089e7a42c8b5882e1af3d71f9")
    //    val appKeys = Array("6afea8f14e72656cf01cf876cf7a4391")


    val dataArray = Array("0303", "0302", "0301", "0229", "0228", "0227", "0226", "0225", "0224", "0223", "0222", "0221", "0220", "0219", "0218", "0217", "0216", "0215", "0214", "0213", "0212", "0211", "0210", "0209", "0208", "0207", "0206", "0205", "0204", "0203", "0202", "0201")


    val gcmdIDPath0 = "s3n://xinmei-ad-log/channel_duid_gcmid/channel_duid_gcmid_mapping_20160304"
    var totalGcmID: RDD[(String, (String, String, String, String, String))] = sc.textFile(gcmdIDPath0)
      .map { x =>
      val item = x.split("\t")

      val duid = item(1)
      val gcmdid = item(2)
      val themeKey = item(3)
      val chanel = item(4)
      val pkgname = item(5)
      (duid, (themeKey, gcmdid, chanel, pkgname, "0304"))
    }
      .filter { x => themeAppKeys.contains(x._2._1) }
      .map { x => (x._1, x._2) }



    for (data0 <- dataArray) {
      val gcmdIDPath = "s3n://xinmei-ad-log/channel_duid_gcmid/channel_duid_gcmid_mapping_2016" + data0
      println("gyy-log: gcmdIDPath : " + gcmdIDPath)

      val gcmidData = sc.textFile(gcmdIDPath)
        .map { x =>
        val item = x.split("\t")

        val duid = item(1)
        val gcmdid = item(2)
        val themeKey = item(3)
        val chanel = item(4)
        val pkgname = item(5)

        (duid, (themeKey, gcmdid, chanel, pkgname, data0))
      }
        .filter { x => themeAppKeys.contains(x._2._1) }
        .map { x => (x._1, x._2) }
        .distinct()


      println("gyy-log: date: " + data0 + "\t" + " gcmdid count: " + gcmidData.count())


      totalGcmID = totalGcmID.union(gcmidData)

    }


    println("gyy-log: before group : totalGcmID: " + totalGcmID.count())

    val totalGcmID0 =
      totalGcmID
        .groupByKey()
        .map { x =>

        val key = x._1

        var maxData0 = "0"

        var content0 = ("", "", "", "")

        for (item <- x._2) {
          if (maxData0 < item._5) {
            content0 = (item._1, item._2, item._3, item._4)
            maxData0 = item._5
          }
        }
        (key, content0)
      }
        .distinct()


    println("gyy-log: after group : totalGcmID0: " + totalGcmID0.count())


    //      val hahaha = totalGcmID0
    //      .map{x => x._1}
    //      .distinct()
    //      .map{x =>
    //        val item = x.split("_")
    //
    //        (item(1), 1)
    //      }
    //
    //
    //      .reduceByKey(_+_)
    //      .toLocalIterator.toArray
    //
    //      for (haha <- hahaha)
    //      {
    //        println("gyy-log: log.count() : " + haha._1 + "\t" + haha._2)
    //      }




    for (appKey <- appKeys) {

      println("gyy-log: appKey Now: " + appKey)

      //      val wordS3Path = "s3n://emojikeyboardlite/word/20160202/language=en_US/*"

      println("gyy-log: gcmidData.count() : " + totalGcmID0.count())

      val metaFullPath = "hdfs:///user/hive/warehouse/service_full/app_key=" + appKey + "/cdate=20160308/*"

      val testPath = "hdfs:///gaoy/estest/"
      HDFS.removeFile(testPath)
      val metaData = sc.textFile(metaFullPath)
        .map { x =>
        val item = x.split("\t")
        val duid = item(0)
        val version = item(8)
        val lang = item(10)
        val nation = item(6)
        val platform = item(1)
        val androidVersion = item(3)
        val factory = item(9)
        val log_last_date = item(19)
        (duid, (duid, appKey, version, lang, nation.toLowerCase, platform, androidVersion, factory, log_last_date))
      }

      println("gyy-log: metaData.count() : " + metaData.count())


      val finalDataInter = metaData
        //      .fullOuterJoin(totalGcmID0)
        .join(totalGcmID0)

        .map { case (key, ((duid, appKey, version, lang, nation, platform, androidVersion, factory, log_last_date), (themeKey, gcmdid, chanel, pkgname))) =>

        val result = Map(
          "duid" -> duid,
          "version" -> version,
          "lang" -> lang,
          "nation" -> nation,
          "platform" -> platform,
          "androidVersion" -> androidVersion,
          "factory" -> factory,
          "key" -> key,
          "appKey" -> themeKey,
          "lastLoginDate" -> log_last_date,
          "gcmId" -> gcmdid,
          "channel" -> chanel,
          "pkgName" -> pkgname,
          "input" -> appKey
        )
        result
      }


      // hdfs:///user/hive/warehouse/service_full/app_key=" + appKey + "/cdate=20160308/*"
      //appKey in ()
      //Array("78472ddd7528bcacc15725a16aeec190","e2934742f9d3b8ef2b59806a041ab389","4e5ab3a6d2140457e0423a28a094b1fd","34c0ab0089e7a42c8b5882e1af3d71f9")

      println("gyy-log: finalDataInter.count() : " + finalDataInter.count())


      finalDataInter.saveToEs("t_user_3/logs")


    }


  }


}