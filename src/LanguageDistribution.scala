import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import scala.collection.mutable.ArrayBuffer
import com.maxmind.geoip2._
import com.maxmind.geoip2.DatabaseReader
import com.maxmind.db.NoCache
import java.io.File
import java.net.InetAddress
import com.fasterxml.jackson.databind.node.ArrayNode

import com.fasterxml.jackson.databind._

/**
 * @author xm002
 */

object LanguageDistribution {
  
  
    def main(arg : Array[String]){
    
  
    val conf = new SparkConf()
    val sc = new SparkContext(conf)

    
    val database = new File("/home/hadoop/GeoIP2-City/GeoIP2-City.mmdb")
    
    val reader = new com.maxmind.geoip2.DatabaseReader.Builder(database).build()
    
    val ipAddress = InetAddress.getByName("128.101.101.101")
    
    val response = reader.city(ipAddress);
    
    
    
    val metaFullPath = "hdfs:///user/hive/warehouse/service_daily/app_key=34c0ab0089e7a42c8b5882e1af3d71f9/cdate=20160314/*"
    
    val path = "hdfs:///gaoy/ld/"
      
    HDFS.removeFile(path)
    
    val userData = sc.textFile(metaFullPath)
    .map{ x =>
      
      val item = x.split("\t")
      
      val duid = item(0)
      val nation = item(6)
      val language = item(10)
      val extra = item(15)
      
      
      
      val startIndex = extra.indexOf("\"ip\":")
      
      
      
      
      
      
      var ip = "unknown"
      
      if (startIndex > -1)
      {
        var tmp = extra.substring(startIndex)
        val endIndex = extra.indexOf("\",")
        
        if (endIndex > -1)
        {
          ip = tmp.substring(0, endIndex)
        }
      }
        
      
      (duid, ip)      
      
    }
    
    .repartition(1)
    .saveAsTextFile(path)
    
    
    
    
    
    
    
    sc.stop()
    
    
    
    
    }
  
}