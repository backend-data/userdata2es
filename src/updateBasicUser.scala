import org.apache.spark.SparkContext    
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

import org.elasticsearch.spark._ 

import org.elasticsearch.spark.rdd.EsSpark

import scala.collection.immutable.Map
/**
 * @author xm002
 */


object updateBasicUser {
  
  
  def main(args: Array[String]) {
    
    val conf = new SparkConf()
    conf.set("es.index.auto.create", "false")
      .set("es.nodes", "172.31.32.4")
      .set("es.mapping.id","key")
      .set("es.batch.size.entries","1")
      .set("es.write.operation","upsert")
      
      
    val sc = new SparkContext(conf)
    
    
//    val appKeys = Array("d7b10839af1ba26bc4b64881501e6df0","6afea8f14e72656cf01cf876cf7a4391") // theme keys
//    val appKeys = Array("78472ddd7528bcacc15725a16aeec190","e2934742f9d3b8ef2b59806a041ab389","4e5ab3a6d2140457e0423a28a094b1fd","34c0ab0089e7a42c8b5882e1af3d71f9")
//    val appKeys = Array("34c0ab0089e7a42c8b5882e1af3d71f9")
    val appKeys = Array("34c0ab0089e7a42c8b5882e1af3d71f9")
    
    for (appKey <- appKeys)
    {
          
      println("gyy-log: appKey Now: " + appKey)
      val metaDailyPath = "hdfs:///user/hive/warehouse/service_daily/app_key=" + appKey + "/cdate=20160302/*"
      val metaFullPath = "hdfs:///user/hive/warehouse/service_full/app_key=" + appKey + "/cdate=20160302/*"
      
      
      
      
      val testPath = "hdfs:///gaoy/estest/"
      HDFS.removeFile(testPath)
      val metaData = sc.textFile(metaFullPath)
      .map{x => 
        val item = x.split("\t")
        val duid = item(0)
        val version = item(8)
        val lang = item(10)
        val nation = item(6)
        val platform = item(1)
        val androidVersion = item(3)
        val factory = item(9)
        val log_last_date = item(19)
        val extra = item(15)
        val key = duid + "_" + appKey
        
        val result = Map(
            "duid"->duid,
            "version"->version,
            "lang" -> lang,
            "nation" -> nation,
            "platform" -> platform,
            "androidVersion" -> androidVersion,
            "factory" -> factory,
            "key" -> key,
            "appKey" -> appKey,
            "lastLoginDate"-> log_last_date
            
        )
        
        
        var builtin = ""
        
        val startIndex = extra.indexOf("in_builtin")
        if (startIndex > -1)
        {
          val builtintmp = extra.substring(startIndex + 12)
          
          val endIndex = builtintmp.indexOf("]")
          
          if (endIndex > -1)
          {
            builtin = builtintmp.substring(0 , endIndex)
          }
        }
        
        
        var customer = ""
        
        val startIndex0 = extra.indexOf("in_full")
        if (startIndex0 > -1)
        {
          val customertmp = extra.substring(startIndex0 + 9)
          
          val endIndex = customertmp.indexOf("]")
          
          if (endIndex > -1)
          {
            customer = customertmp.substring(0, endIndex)
          }
        }
        
        (builtin, customer)

      }
      .saveAsTextFile(testPath)
      
      
//      .saveToEs("basic_user/logs")
      
      

  
    }
    
    
//  deviceuid           
//platform            
//networktype         
//osversion           
//telecomoperator     
//resolution          
//nation              
//modelname           
//appversion          
//manufacturername    
//language            
//agentversion        
//productname         
//third_account       
//channel             
//extra               
//first_time          
//last_time           
//log_first_date      
//log_last_date       
//service_first_date  
//service_last_date   
//app_key             
//cdate 
    

    
    
    //deviceuid  0           
//platform   1       
//networktype  2        
//osversion     3      
//telecomoperator   4     
//resolution         5 
//nation              6
//modelname           7
//appversion          8
//manufacturername    9
//language            10
//agentversion        11
//productname         12
//third_account       13
//channel             14
//extra               15
//first_time          16
//last_time           17
//log_first_date      18
//log_last_date       19
//app_key             20
//cdate                21
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//    
////    conf.set("es.index.auto.create", "true")
////    conf.set("es.index.auto.create", "true")
//    conf.set("es.nodes", "172.31.28.109")
//    .set("es.mapping.id","_id")
//    
//    
//    
//    case class Trip(id: String, haha: String,hehe:String)
//    
//    val numbers = Trip("1","haha1","hehe1")
////    .map{x => ("_id": "test1", "_source": x)} 
//    val airports = Trip("2","haha2","hehe2") 
//    
////    val result = 
//      
//      
//      sc.makeRDD(Seq(numbers, airports)).map { x =>  
//        
//      val value = x.haha + "_" + x.id
//      val ss = Map("_id"-> value, "_source"->x)
//      ss
//    }
//    
//    .saveToEs("spark/docs")
    

  
  }
  

  
  
  
  
}