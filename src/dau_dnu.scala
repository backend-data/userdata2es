import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import scala.collection.mutable.ArrayBuffer


/**
 * @author xm002
 */
object dau_dnu {
  
  
   val app2verion = Map("34c0ab0089e7a42c8b5882e1af3d71f9"-> "5.0.0",
                          "78472ddd7528bcacc15725a16aeec190"->"4.3.0",
                          "4e5ab3a6d2140457e0423a28a094b1fd"-> "3.2.5",
                          "e2934742f9d3b8ef2b59806a041ab389"->"4.3.7"
                          )
  
  
   def getUserInfo(dauPath: String,dnuPath: String,meta_daily: String,sc: SparkContext,nation: String,app: String)
   {
     
     val dauUser = sc.textFile(dauPath).map { x => (x,("","")) }
     
     val newUser = sc.textFile(dnuPath)
     .map{x => 
       val item = x.split("\t")
       (item(0), item(1), item(4).toLowerCase())
     }
     .filter{x => x._3 == nation}
     
     
     
     val allUser = sc.textFile(meta_daily)
     .map{x => 
       val item = x.split("\t")
       (item(0),(item(8),item(6).toLowerCase()))
     }
     .filter{x => x._2._2 == nation}

     .join(dauUser)
     .map{x => 
       
       (x._1, x._2._1._1, x._2._1._2.toLowerCase())
     }
     
     
     val oldUser = allUser.subtract(newUser)
     
     println("gyy-log: " + app)
     println("gyy-log: oldUser: " + oldUser.count())
     
     println("gyy-log: newUser: " + newUser.count())
     
     val oldpath = "hdfs:///gaoy/dau_dnu/" + app + "_oldUser_" + nation
     HDFS.removeFile(oldpath)
     oldUser.map{x => x._1 + "\t" + x._2 + "\t" + x._3}.repartition(1).saveAsTextFile(oldpath)
     
     val newpath = "hdfs:///gaoy/dau_dnu/" + app + "_newUser_" + nation
     HDFS.removeFile(newpath)
     newUser.map{x => x._1 + "\t" + x._2 + "\t" + x._3}.repartition(1).saveAsTextFile(newpath)
     

   }
   
   
   def interUsers(oldpath: String,newpath: String,callbackPath: String, sc: SparkContext,app: String){
     val callback = sc.textFile(callbackPath)
      .map{x => 
        val item = x.split("\t")
        (item(1), item(2), item(3), item(4))
      }
      .filter{ case (appkey, duid, appversion, payout) => appkey == app}
      .map{ case (appkey, duid, appversion, payout) => (duid, payout.toDouble) }
      
     
      val newUser = sc.textFile(newpath)
      .map{ x=>
        val item = x.split("\t") 
      
         (item(0),-1.0)
      }
      
      val oldUser = sc.textFile(oldpath)
      .map{ x=>
        val item = x.split("\t")  
         (item(0),-1.0)
      }
      
      
      val new_hahha = newUser.join(callback)
      println("gyy-log: newUser.intersection(callback).count() " + new_hahha.count())
      
      val new_result = new_hahha.map{x => 
        val payout = x._2._2  
      (1, payout)
      }.reduceByKey(_+_).toLocalIterator.toArray
      
      if (new_result.length == 0)
        println("gyy-log: new_result ")
      else
        println("gyy-log: new_result " + new_result(0)._2)
          


      
      
      val old_hahha = oldUser.join(callback)
      println("gyy-log: oldUser.intersection(callback).count() " + old_hahha.count())
      
      
       val old_result = old_hahha.map{x => 
        val payout = x._2._2  
      (1, payout)
      }.reduceByKey(_+_).toLocalIterator.toArray
      
      if (old_result.length == 0)
        println("gyy-log: old_result ")
      else
        println("gyy-log: old_result " + old_result(0)._2)
      
      
     
     
   }
   
   
   
 
   
   
   
    def calPerRevenue(oldpath: String,newpath: String,callbackPath: String, sc: SparkContext,app: String){
      
      val callback = sc.textFile(callbackPath)
      .map{x => 
        val item = x.split("\t")
        (item(1), item(2), item(3), item(4))
      }
      .filter{ case (appkey, duid, appversion, payout) => appkey == app}
      
      
      val versionBoundary = app2verion.get(app).get
      //step 0. process New users first.
        // process new-version and old-version at the same time. 
      
      println("gyy-log: newpath: " + newpath)
      
//      (409c0b1105e0d05a41d629fc8ab710b2,5.3.8,us)
      val newUser = sc.textFile(newpath)
      .map{ x=>
        val item = x.split("\t") 
      
         (item(0), item(1), item(2))
      }
      
      
      
     val newVersionInNewUsers = newUser.filter { case (duid, appversion, nation) => (appversion >=  versionBoundary)}
     val oldVersionInNewUsers = newUser.filter { case (duid, appversion, nation) => (appversion <  versionBoundary)}
     
     println("\n")
     println("gyy-log:" + "\t" + app)
     println("gyy-log:" + "\t" + "newVersionInNewUsers: " + newVersionInNewUsers.count())
     println("gyy-log:" + "\t" + "oldVersionInNewUsers: " + oldVersionInNewUsers.count())
     
     val newVersionInCallback_cp = callback.filter{ case (appkey, duid, appversion, payout) => appversion >= versionBoundary}
     
     val newVersionInCallback= newVersionInCallback_cp
     .map{ case (appkey, duid, appversion, payout) =>
       
       (1, payout.toFloat)
     }.reduceByKey(_+_).toLocalIterator.toArray
     
     
     
     val oldVersionInCallback_cp = callback.filter{ case (appkey, duid, appversion, payout) => appversion < versionBoundary}
     
     val oldVersionInCallback = oldVersionInCallback_cp
     .map{ case (appkey, duid, appversion, payout) =>
       
       (1, payout.toFloat)
     }.reduceByKey(_+_).toLocalIterator.toArray
     
     println("gyy-log:" + "\t" + "newVersionInCallback: " + newVersionInCallback(0)._2)
     println("gyy-log:" + "\t" + "oldVersionInCallback: " + oldVersionInCallback(0)._2)
     
     
     // callback generate by dnu
     val tmp = newVersionInNewUsers.map{ case (duid, appversion, nation) =>
          (duid, 1)
     }
     val callbackForDnuForNewVersion = newVersionInCallback_cp.map{case (appkey, duid, appversion, payout) => (duid, payout)}.join(tmp)
     .map{x => (1, x._2._1.toFloat)}
     .reduceByKey(_+_).map{x => x._2}.toLocalIterator.toArray
     
     if(callbackForDnuForNewVersion.length == 0)
       println("gyy-log:" + "\t" + "callbackForDnuForNewVersion: special 0")
     else{
       println("gyy-log:" + "\t" + "callbackForDnuForNewVersion: " + callbackForDnuForNewVersion(0))  
       }
     
     
     val tmp1 = oldVersionInNewUsers.map{ case (duid, appversion, nation) =>
          (duid, 1)
     }
     val callbackForDnuForOldVersion = oldVersionInCallback_cp.map{case (appkey, duid, appversion, payout) => (duid, payout)}.join(tmp1)
     .map{x => (1, x._2._1.toFloat)}
     .reduceByKey(_+_).toLocalIterator.toArray
     
     if (callbackForDnuForOldVersion.length == 0)
       println("gyy-log:" + "\t" + "callbackForDnuForOldVersion: special  0")
     else
       {println("gyy-log:" + "\t" + "callbackForDnuForOldVersion: " + callbackForDnuForOldVersion(0)._2)}

           
      
      //step 1 process old users second
     
      val oldUser = sc.textFile(oldpath)
      .map{ x=>
        val item = x.split("\t")  
      
         (item(0), item(1), item(2))
      }
      
      
      
     val newVersionInOldUsers = oldUser.filter { case (duid, appversion, nation) => (appversion >=  versionBoundary)}
     val oldVersionInOldUsers = oldUser.filter { case (duid, appversion, nation) => (appversion <  versionBoundary)}
     
     println("\n")
     println("gyy-log:" + "\t" + app)
     println("gyy-log:" + "\t" + "newVersionInOldUsers: " + newVersionInOldUsers.count())
     println("gyy-log:" + "\t" + "oldVersionInOldUsers: " + oldVersionInOldUsers.count())
     
     
     
     // callback generate by dnu
     val tmp2 = newVersionInOldUsers.map{ case (duid, appversion, nation) =>
          (duid, 1)
     }
     val callbackForDauForNewVersion = newVersionInCallback_cp.map{case (appkey, duid, appversion, payout) => (duid, payout)}.join(tmp2)
     .map{x => (1, x._2._1.toFloat)}
     .reduceByKey(_+_).toLocalIterator.toArray
     
     if (callbackForDauForNewVersion.length == 0)
       println("gyy-log:" + "\t" + "callbackForDauForNewVersion: special 0")
     else
       println("gyy-log:" + "\t" + "callbackForDauForNewVersion: " + callbackForDauForNewVersion(0)._2)
     
     
     val tmp3 = oldVersionInNewUsers.map{ case (duid, appversion, nation) =>
          (duid, 1)
     }
     val callbackForDauForOldVersion = oldVersionInCallback_cp.map{case (appkey, duid, appversion, payout) => (duid, payout)}.join(tmp3)
     .map{x => (1, x._2._1.toFloat)}
     .reduceByKey(_+_).toLocalIterator.toArray
     
     if(callbackForDauForOldVersion.length == 0)
       println("gyy-log:" + "\t" + "callbackForDauForOldVersion: special 0")
     else  
       println("gyy-log:" + "\t" + "callbackForDauForOldVersion: " + callbackForDauForOldVersion(0)._2)
       
      
    }
  
    def main(arg : Array[String]){
    
    val nation = arg(0)
    val conf = new SparkConf()
    val sc = new SparkContext(conf)
    
    println("gyy-log nation NOW: " + nation)
    
   
    
    val appKey = Array("e2934742f9d3b8ef2b59806a041ab389","34c0ab0089e7a42c8b5882e1af3d71f9","78472ddd7528bcacc15725a16aeec190","4e5ab3a6d2140457e0423a28a094b1fd")
//    val appKey = Array("34c0ab0089e7a42c8b5882e1af3d71f9")
    
    for (app <- appKey)
    {
      val dauPath = "hdfs:///user/hive/warehouse/dau/app_key=" + app + "/cdate=20160217/*"
      val dnuPath = "hdfs:///user/hive/warehouse/dnu/app_key=" + app + "/cdate=20160217/*"
      val meta_daily = "hdfs:///user/hive/warehouse/meta_daily/app_key=" + app + "/cdate=20160217/*"
      
      getUserInfo(dauPath: String,dnuPath: String,meta_daily: String,sc: SparkContext,nation: String,app: String)
      
    }
    
    
    for (app <- appKey)
    {
      println("gyy-log " + app)
      val oldpath = "hdfs:///gaoy/dau_dnu/" + app + "_oldUser_" + nation + "/*"
      val newpath = "hdfs:///gaoy/dau_dnu/" + app + "_newUser_" + nation + "/*"
      val callbackPath = "hdfs:///gaoy/dau_dnu/" + nation
      
      interUsers(oldpath: String,newpath: String,callbackPath: String, sc: SparkContext,app: String)
      
      
    }
   
    
    sc.stop()
    
    
    
  }
  
}