import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import scala.collection.mutable.ArrayBuffer


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * @author xm002
 */
object platform_count {
  
  def keywordFilter(sc: SparkContext,adLog: String, keyString: String){
    val log = sc.textFile(adLog)
    .filter{x => x.contains(keyString)}
//    .filter{x => x.contains("lite_popup_news_details_bottom_a") && !x.contains("lite_popup_news_details_bottom_ad")}
    .distinct()
    
    val outputDir = "hdfs:///gaoy/keywordFilter/"
    HDFS.removeFile(outputDir)
    log.repartition(1).saveAsTextFile(outputDir)
    
  }
  
  def platformCount(sc: SparkContext,wordS3Path: String){
    val word = sc.textFile(wordS3Path)
    .map{x => 
      val item = x.split("\t")
      
      if (item.length > 1)
        item(0)
      else
        ""
    }
    .filter{ x => x != ""}
    .map{x => (x, 1)}
    .reduceByKey(_+_)
    .map{x => x._1 + "\t" + x._2}
    
    val outputDir = "hdfs:///gaoy/platform_count/"
    HDFS.removeFile(outputDir)
    word.repartition(1).saveAsTextFile(outputDir)
    
    
  }
  
  def cityCount(sc: SparkContext,wordS3Path: String){
    
    val cityList = sc.broadcast((sc.textFile("hdfs:///gaoy/AmericaCityList").map{x => x.toLowerCase()}.toArray()))
    
    
    val word = sc.textFile(wordS3Path)
    .flatMap{x =>
      val item = x.split("\t")
      var city0 = new ArrayBuffer[(String, String)]()
      if (item.length > 1) 
      {
        val haha = item(1).toLowerCase()
        for(city <- cityList.value) 
        {
          if (haha.contains(city)) 
          {
            val item0 = (city, item(1))
            city0 +=  item0
          } 
        }
      }    
      city0
    }
    .groupByKey()
    .zipWithIndex()
    .map{x => 
      x._2 + "\n" + x._1._1 + "\n" + x._1._2.mkString("\n")   + "\n"
    }
    
    val cityCountDir = "hdfs:///gaoy/cityCount/"
    HDFS.removeFile(cityCountDir)
    
    word.repartition(1).saveAsTextFile(cityCountDir)
    
    
    
    
  }
  
  
  def appGenderFrequencyHistogram(sc: SparkContext, facebookPath:String, metaFull:String){
    
    
//    (6,7efaa8fcd9b4843921d43acdc014fc81,10a5622e23d93d984350be37dfb0740a,{"id":"1197020996982046","name":"Aaquib Mohammed Ismail","link":"https:\/\/www.facebook.com\/app_scoped_user_id\/1197020996982046\/","gender":"male","locale":"en_US","updated_time":"2015-09-23T00:34:09+0000","timezone":8,"verified":true,"age_range":{"min":21}})
    
    val facebookData = sc.textFile(facebookPath)
    .map{x => 
      var item = x.replaceAll("\\(|\\)","")
      
      val index0 = item.indexOf(",")
      
      item = item.substring(index0 + 1, item.length())
      
      val index1 = item.indexOf(",")
      
      val duid = item.substring(0, index1)
      
      val index2 = item.indexOf(",")
      
      item = item.substring(index2 + 1, item.length())
      
      val index3 = item.indexOf(",")
      
      val userInfo = item.substring(index3 + 1, item.length)
      
      val om = new ObjectMapper()
      
      var gender = "unknown"
      try
      {
        val jn = om.readTree(userInfo)
        gender = jn.get("gender").textValue()
      }
      catch {
        case _: Throwable => 
          gender = "wrong"
      }
      (duid, gender)
    }
    .filter{x => x._2 != "wrong"}
    
    
    val result = sc.textFile(metaFull)
    .map{x =>
      
      val item = x.split("\t")
      val duid = item(0)
      val extra = item(15)
      
      val nation = item(6)
      
      val om = new ObjectMapper()
      var appList = ""
      val inFullIndex0 = extra.indexOf("in_full=")
      
      
      var inFull = ""
      if (inFullIndex0 >= 0)
      {
        inFull = extra.substring(inFullIndex0 + 9)
        val inFullIndex1 = inFull.indexOf("]")
        
        if (inFullIndex1 >= 0)
          inFull = inFull.substring(0, inFullIndex1)
        else
          inFull = "wrong"
          
      }   
      else
        inFull = "wrong"

      (duid,(inFull,nation))
    }
    .filter{x => x._2._1 != "wrong"}
    .join(facebookData)
//    .filter{x => x._2._1._2.toLowerCase() == "us"}
    
    
    
    
    
   
    
    
    val histo = result.map{case (duid, ((appList, nation), gender)) => (duid, appList, gender)}
    
    val menCount = histo.filter{x => x._3 == "male"}.count()
    
    val womenCount = histo.filter{x => x._3 == "female"}.count()
    
    
    
//    val menPercent = menCount.toFloat /(menCount + womenCount).toFloat
//    val womenPercent = womenCount.toFloat /(menCount + womenCount).toFloat
    
    
    val menPercent = menCount.toFloat 
    val womenPercent = womenCount.toFloat 
    
    println("gyy-log: menCount: " + menCount)
    println("gyy-log: womenCount: " + womenCount)
    
    
    
    
    
    val result0 = histo
    
    
    .flatMap{ case (duid, appList, gender) =>
      
     var genderLabel = (0, 1)
     
     if (gender == "male")
       genderLabel = (1, 0)   // (male, female)
      
     appList.replaceAll("\\s+", "").split(",").map{x => (x, genderLabel)}
     
    }
    .reduceByKey{ 
      (a: (Int, Int), b: (Int, Int)) =>
          (a._1 + b._1, a._2 + b._2)
    }
    
    .map{ case (pkgname, (menNum, womenNum)) =>
      
      
      var ratio  = -1f
      try{
        ratio = (menNum.toFloat/menPercent)/(womenNum.toFloat/womenPercent)  
      }
      catch {
          case _: Throwable => ratio = -1
      }
      
      
      
       pkgname + "\t" + menNum + "\t" + menNum.toFloat/menPercent + "\t" + womenNum + "\t" +  womenNum.toFloat/womenPercent + "\t" + ratio
    }
    
//    .map{ x=>
//      
//      x._1 + "\t" + x._2._1 + "\t" + x._2._2
//    }
//    .sortBy(f, ascending, numPartitions)
//    
    .repartition(1)
//    .sortBy(x => x._2._1)
    
    
    val path = "hdfs:///gaoy/genderTmp/"
    HDFS.removeFile(path)
    
    result0
   .saveAsTextFile(path)
    
  }
  
  
  
  
  def wordGroup(sc: SparkContext,wordS3Path: String){
    
    val word = sc.textFile(wordS3Path)
    .map{x => 
      val item = x.split("\t")
      
      if (item.length > 3)
        (item(2),(item(0),item(1), item(3)))
      else
         ("",("","",""))
    }
    .filter{x => x._2._1 != "" && x._2._2 != ""}
    .groupByKey()
    .map{x => 
       val haha = x._2.toList.sortBy(_._3).mkString("\n") 
       x._1 + "\n\n" + haha
    }

    
    
//    val categoryRef = "hdfs:///gaoy/lite_platform_category.txt"
//    val category = sc.textFile(categoryRef)
//    .map{x => 
//      val item = x.split("\t")
//      (item(0), item(1))
//    }
//    
//    
//    
//    val finalWord = word.join(category)  // RDD[(String, (Iterable[String], String))]
//        .filter{x => x._2._2 != "None"}
//        .map{x => (x._2._2.toUpperCase(), (x._1, x._2._1.mkString("\n\n")))}
//        .groupByKey()
//        .map{x => x._1 + "\n" + x._2.mkString("\n\n\n")}
        
        
    val outputDir = "hdfs:///gaoy/wordGroup/"
    HDFS.removeFile(outputDir)
    word.repartition(1).saveAsTextFile(outputDir)
    
    
    
  }
  
  
  def main(arg : Array[String]){
    
  
    val conf = new SparkConf()
    val sc = new SparkContext(conf)
    
//    val hadoopConf = sc.hadoopConfiguration;
//    hadoopConf.set("fs.s3n.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
//    hadoopConf.set("fs.s3n.awsAccessKeyId", "AKIAJIVCMRHJ56IM7IVA")
//    hadoopConf.set("fs.s3n.awsSecretAccessKey", "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")
//    
////    val wordS3Path = "s3n://emojikeyboardlite/word/20160202/language=en_US/*"
//    
//    val adLogPath = "s3n://xinmei-ad-log/ad/ad.log.skip*" + "20160217" + "*"
//    val keyString = "lite_popup_news_details_bottom_ad"
//    
////    wordGroup(sc,wordS3Path)
//    
////    cityCount(sc,wordS3Path)
//    
//    keywordFilter(sc: SparkContext,adLogPath: String, keyString: String)
    
    val facebookPath = "hdfs:///gaoy/fb/part-00000"
    val metaFull = "hdfs:///user/hive/warehouse/meta_full/app_key=*/cdate=20160217/*"
    appGenderFrequencyHistogram(sc: SparkContext, facebookPath:String, metaFull:String)
    
    sc.stop()
    
    
    
  }
  
  
  
  
  
      
}